package com.example.laptrinh.myapplication;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.EditText;

import java.util.Date;
import java.util.List;
import java.util.concurrent.TimeUnit;

import io.reactivex.Observable;
import io.reactivex.ObservableEmitter;
import io.reactivex.ObservableOnSubscribe;
import io.reactivex.ObservableSource;
import io.reactivex.Observer;
import io.reactivex.Scheduler;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.functions.Consumer;
import io.reactivex.functions.Function;
import io.reactivex.schedulers.Schedulers;
import io.realm.Realm;
import io.realm.RealmList;
import io.realm.RealmResults;

public class MainActivity extends AppCompatActivity implements ChatAdapter.IChatAdapter, View.OnClickListener {
    private RealmResults<ItemMessage> itemMessages;
    private Realm realm;
    private RecyclerView rcChat;
    private EditText edtMessage;
    private Disposable disposable;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        realm = Realm.getDefaultInstance();
        itemMessages =
                realm.where(ItemMessage.class).findAll();

        initView();
        demoRxMessage();
    }

    private void demoRxMessage() {
        disposable =
                Observable
                        .create(new ObservableOnSubscribe<ItemMessage>() {
                            @Override
                            public void subscribe( ObservableEmitter<ItemMessage> emitter) throws Exception {
                                ItemMessage message = new ItemMessage();
                                message.setContent("Hello");
                                message.setReceiver("My");
                                message.setSender("Friend");
                                message.setCreatedTime(new Date());

                                emitter.onNext(message);
                                emitter.onComplete();
                            }
                        })
                        .repeatWhen(new Function<Observable<Object>, ObservableSource<?>>() {
                            @Override
                            public ObservableSource<?> apply(Observable<Object> objectObservable) throws Exception {
                                return objectObservable.delay(2, TimeUnit.SECONDS);
                            }
                        })
                        .subscribeOn(Schedulers.newThread())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe(new Consumer<ItemMessage>() {
                            @Override
                            public void accept(ItemMessage itemMessage) throws Exception {
                                realm.beginTransaction();
                                realm.insert(itemMessage);
                                realm.commitTransaction();

                                rcChat.getAdapter().notifyItemInserted(itemMessages.size() - 1);
                                rcChat.smoothScrollToPosition(itemMessages.size() - 1);
                            }
                        });

    }

    @Override
    protected void onDestroy() {
        disposable.dispose();
        super.onDestroy();
    }

    private void initView() {
        rcChat = (RecyclerView) findViewById(R.id.rc_chat);

        LinearLayoutManager  manager = new LinearLayoutManager(this);
        manager.setStackFromEnd(true);
        rcChat.setLayoutManager(manager);
        rcChat.setAdapter(new ChatAdapter(this));
        findViewById(R.id.btn_send).setOnClickListener(this);

        edtMessage = (EditText) findViewById(R.id.edt_message);
    }

    @Override
    public int getCount() {
        if (itemMessages == null) {
            return 0;
        }
        return itemMessages.size();
    }

    @Override
    public ItemMessage getItemMessage(int position) {
        return itemMessages.get(position);
    }

    @Override
    public void onClick(View view) {
        String content = edtMessage.getText().toString();
        ItemMessage message = new ItemMessage();
        message.setContent(content);
        message.setReceiver("Friend");
        message.setSender("My");
        message.setCreatedTime(new Date());

        realm.beginTransaction();
        realm.insert(message);
        realm.commitTransaction();

        rcChat.getAdapter().notifyItemInserted(itemMessages.size() - 1);
        rcChat.smoothScrollToPosition(itemMessages.size() - 1);

    }
}
