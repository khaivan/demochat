package com.example.laptrinh.myapplication;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;


public class ChatAdapter extends
        RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private IChatAdapter inter;

    public ChatAdapter(IChatAdapter inter) {
        this.inter = inter;
    }

    @Override
    public int getItemViewType(int position) {
        return getType(position);
    }

    private int getType(int position){
        ItemMessage message = inter.getItemMessage(position);
        if (message.getSender().equals("My")) {
            return 0;
        } else {
            return 1;
        }
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent,
                                             int viewType) {
        View view;
        LayoutInflater inflater =
                LayoutInflater.from(parent.getContext());
        if ( viewType == 0 ) {
            view = inflater.inflate(R.layout.item_send, parent, false);
            return new ViewHolderChatSend(view);
        }else {
            view = inflater.inflate(R.layout.layout_receive, parent, false);
            return new ViewHolderChatReceiver(view);
        }

    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        int type = getType(position);
        ItemMessage message = inter.getItemMessage(position);
        switch (type){
            case 0:
                ViewHolderChatSend h = (ViewHolderChatSend)holder;
                h.tvMessage.setText(message.getContent());
                break;
            default:
                ViewHolderChatReceiver hR = (ViewHolderChatReceiver)holder;
                hR.tvMessage.setText(message.getContent());
                break;
        }
    }

    @Override
    public int getItemCount() {
        return inter.getCount();
    }

    static class ViewHolderChatSend extends RecyclerView.ViewHolder {
        private TextView tvMessage;

        public ViewHolderChatSend(View itemView) {
            super(itemView);
            tvMessage = (TextView) itemView.findViewById(R.id.tv_message);
        }
    }

    static class ViewHolderChatReceiver extends RecyclerView.ViewHolder {
        private TextView tvMessage;

        public ViewHolderChatReceiver(View itemView) {
            super(itemView);
            tvMessage = (TextView) itemView.findViewById(R.id.tv_message);
        }
    }

    public interface IChatAdapter {
        int getCount();

        ItemMessage getItemMessage(int position);
    }
}
