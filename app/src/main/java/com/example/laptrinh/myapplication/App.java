package com.example.laptrinh.myapplication;

import android.app.Application;
import android.content.Context;

import io.realm.Realm;

public class App extends Application {
    private static Context context;
    @Override
    public void onCreate() {
        super.onCreate();
        context = this;
        Realm.init(this);
    }

    public static Context getContext(){
        return context;
    }
}
